package duma.dragos;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.io.*;


public class S2 extends JFrame {

    JTextArea textArea;
    JButton button;
    JTextField textField;
    S2(){
        setTitle("Interface");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(400,500);
        init();
        setVisible(true);

    }

    void init(){

        this.setLayout(null);

        textField= new JTextField();
        textField.setBounds(0,50,200,20);
        add(textField);

        button = new JButton("Display text in text area");
        button.setBounds(0,150,200,20);
        add(button);

        textArea = new JTextArea();
        textArea.setBounds(0,200,200,200);
        add(textArea);

        button.addActionListener(event -> {
            try {
                buttonReaction(event);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

    }

    void buttonReaction(ActionEvent e) throws IOException{

        try {
            String text = textField.getText();
            File f = new File(text);
            BufferedReader fileReader = new BufferedReader(new FileReader(f));
            String string;
            while((string=fileReader.readLine())!=null){
                textArea.append(string);
            }
        }
        catch (IOException ioe){
            System.out.println(ioe.fillInStackTrace());
        }
    }

    public static void main(String[] args) {
        S2 s2 = new S2();
    }
}
