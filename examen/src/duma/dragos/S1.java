package duma.dragos;

public class S1 {

    class A {

    }

    class B extends A{
        C c;
        private String param;
        public void met1(Z z){

        }

        public B(C c) {
            this.c = c;
        }
    }

    class Z{

        public void g(){

        }

    }

    class D{
        B b;
        public void f(){

        }
    }

    class E{
        B b;

        public void setB(B b) {
            this.b = b;
        }
    }

    class C{

    }
}
